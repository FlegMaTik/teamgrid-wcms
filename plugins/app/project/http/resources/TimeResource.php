<?php namespace App\Project\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TimeResource extends Resource
{
    public function toArray($request)
    {
        return [
            '_id' => $this->id,
            'taskId' => $this->task_id,
            'start' => $this->started_at,
            'end' => $this->stopped_at,
            'projectId' => $this->project_id,
            'userId' => $this->user_id,
            'createdBy' => $this->created_by,
            'createdAt' => $this->created_at,

        ];
    }
}
