<?php namespace App\Project\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    public function toArray($request)
    {
        return [
            'statusCode' => 201,
            'status' => 'Created',
            'info' => 'Projects',
            'data' => [
                'name' => $this->name,
                'createdBy' => $this->user_id,
                'createdAt' => $this->created_at,
                'completed' => $this->completed,
            ]
        ];
    }
}

