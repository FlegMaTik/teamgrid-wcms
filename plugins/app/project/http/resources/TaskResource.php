<?php namespace App\Project\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TaskResource extends Resource
{
    public function toArray($request)
    {
        return [
        'name' => $this->name,
            'userId' => $this->user->id,
            'createdAt' => $this->created_at,
            'createdBy' => $this->user->id,
            'projectId' => $this->project_id,
            'listOrder' => $this->list_order,

        ];
    }
}

