<?php namespace App\Project\Http\Controllers;
use App\Project\Http\Resources\TimeResource;
use App\Project\Http\Resources\TaskResource;
use App\Project\Models\Project;
use App\Project\Models\Task;
use App\Project\Models\Time;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Request;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;
use Wezeo\UserApi\Facades\JWTAuth;
use Winter\Storm\Database\Model;

class TimeController
{
    public function index()
    {
        return TimeResource::collection(Time::all());
    }

    public function create()
    {
        $user = JWTAuth::getUser();


        $time = new Time([
            'task_id' => post('task_id'),
            'user_id' => post('user_id'),
            'started_at' => post('started_at'),
            'stopped_at' => post('stopped_at'),

        ]);
        $time->user = $user;
        $time->save();

        return new TimeResource($time);
    }

    public function show($id)
    {
        return new TimeResource(Time::find($id));
    }

    public function store($id)
    {
        $user = JWTAuth::getUser();
        $time = Time::find($id)->update([
            'task_id' => post('task_id'),
            'user_id' => post('user_id'),
            'started_at' => post('started_at'),
            'stopped_at' => post('stopped_at'),
            'created_at' => post('created_at'),
            'created_by' => $user->id,
        ]);
        return response('time', 200);
    }
    public function start($id)
    {
        $user = JWTAuth::getUser();

        $time = Time::make([
            'user_id' => post('user_id'),
            'started_at' => post('started_at'),
            'task_id' => $id,
        ]);

        $time->created_by = User::findOrFail(post('user_id'));
        $time->save();

        return new TimeResource($time);
    }

    public function stop($tid)
    {
        return $tid;
        $user = JWTAuth::getUser();
        $time = Time::whereNull('stopped_at')->where('user_id', $user->id)->first();
        $time->stopped_at = post('stopped_at');
        $time->save();


        return new TimeResource($time);
    }

}
