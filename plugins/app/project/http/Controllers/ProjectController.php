<?php namespace App\Project\Http\Controllers;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Project\Http\Resources\ProjectResource;
use App\Project\Models\Project;
use App\Project\Models\Task;
use Response;
use RainLab\User\Facades\Auth;

class ProjectController
{

    public function index()
    {
        return ProjectResource::collection(Project::all());
    }

    public function create()
    {
        $project = Project::create([
            'name' => post('name'),
            'project_completed' => post('project_completed'),
        ]);
        return $project;
    }

    public function update($id)
    {
        Project::find($id)->update([
            'name' => post('name'),
            'project_completed' => post('project_completed')

        ]);
        return 'updated';
    }

    public function destroy($id)
    {
        Project::find($id)->delete();
        return 'deleted';
    }
}
