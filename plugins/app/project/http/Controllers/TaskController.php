<?php namespace App\Project\Http\Controllers;
use App\Project\Http\Resources\TaskResource;
use App\Project\Models\Project;
use App\Project\Models\Task;
use Wezeo\UserApi\Facades\JWTAuth;

class TaskController
{
    public function index()
    {
        return TaskResource::collection(Task::with([
            'times',
            'project'
        ])->get());
    }

    public function create()
    {
        $user = JWTAuth::getUser();
        $project = Project::findOrFail(post('project_id'));

        $task = new Task([
            'name' => post('name'),
            'list_order' => post('list_order')
        ]);
        $task->user = $user;
        $task->project = $project;
        $task->save();

        return new TaskResource($task);
    }

    public function update($id)
    {
        Task::find($id)->update([
            'project_id' => post('project_id'),
            'name' => post('name'),
            'list_order' => post('list_order'),
            'is_completed' => post('is_completed'),
        ]);

        return 'updated';
    }

    public function destroy($id)
    {
        Task::find($id)->delete();

        return 'deleted';
    }
}
