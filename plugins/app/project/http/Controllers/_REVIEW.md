# 22.5 Halas
[] ked mas controller tak sprav nech extenduje Illuminate\Routing\Controller a potom mozes v routes pouzivat apiResource
[] v ProjectController pouzivaj resource, nie Response::make
[] ked updatujes/deletujes model vrat ho, nevracaj ze updated/deleted ale daj ze return new XxxResource($model);
    [] ale ked tak rozmyslam mozno tak treba kvoli niecomu aby to bolo 1:1 k teamgridu
[] v TaskController pouzivas with(...) ale nasledne ich v resource nevyuzivas + mozno by som to rozdelil na 2 riadky
[] v TaskController si uprav update metodu podla create
    [] Toto vseobecne nenechaj niekomu passnut project_id do requestu a potom ho pouzit radsej daj ze Model::findOrFail('model_id') a potom daj ze $nieco->relation = $model
[] pouzivaj findOrFail ked planujes nieco dalej robit s modelom, lebo ak das iba find a nenajde sa to vyhodi nepekny error
[] vymaz unused namespaces


[_] tak do buducnosti by som rozmyslal nad tym ze mozno vzdy nechcem vsetky tasky ale chcem napriklad tasky len k projektu / aktivne tasky pre uzivatela, lebo momentalne vsade vracias vsetko, takze by som sa zamyslel ze co kde potrebujes pripadne sa s timom poradil ze co vsetko bude treba a podla toho spravil o par controllerov naviac.

