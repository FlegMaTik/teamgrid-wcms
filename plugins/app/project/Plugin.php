<?php namespace App\Project;

use Backend;
use System\Classes\PluginBase;
use RainLab\User\Models\User;

/**
 * project Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'project',
            'description' => 'No description provided yet...',
            'author' => 'app',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        User::extend(function ($model)
        {
            $model->hasMany['tasks'] = ['App\Project\Models\Task'];
            $model->hasMany['times'] = ['App\Project\Models\Time'];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'App\Project\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'app.project.some_permission' => [
                'tab' => 'project',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {


        return [
            'project' => [
                'label' => 'project',
                'url' => Backend::url('app/project/projects'),
                'icon' => 'icon-leaf',
                'permissions' => ['app.project.*'],
                'order' => 500,
            ],
            'task' => [
                'label' => 'task',
                'url' => Backend::url('app/project/tasks'),
                'icon' => 'icon-leaf',
                'permissions' => ['app.project.*'],
                'order' => 500,
            ],
            'time' => [
                'label' => 'time',
                'url' => Backend::url('app/project/times'),
                'icon' => 'icon-leaf',
                'permissions' => ['app.project.*'],
                'order' => 500,
            ],
        ];
    }
}
