<?php
use Wezeo\UserApi\Http\Middlewares\Authenticate;

Route::group(['middleware' => [Authenticate::class]], function ()

{
    Route::get("/tasks", "App\Project\Http\Controllers\TaskController@index");

    Route::get("/projects", "App\Project\Http\Controllers\ProjectController@index");

    Route::get("/times", "App\Project\Http\Controllers\TimeController@index");

    Route::get("/times/{id}", "App\Project\Http\Controllers\TimeController@show");

    Route::post("/tasks/{id}/startTracking", "App\Project\Http\Controllers\TimeController@start");

    Route::post("/task/{id}/stopTracking", "App\Project\Http\Controllers\TimeController@stop");

    Route::post("/projects", "App\Project\Http\Controllers\ProjectController@create" );

    Route::post("/tasks", "App\Project\Http\Controllers\TaskController@create");

    Route::post("/times", "App\Project\Http\Controllers\TimeController@create");

    Route::put("/projects/{id}", "App\Project\Http\Controllers\ProjectController@update");

    Route::put("/tasks/{id}", "App\Project\Http\Controllers\TaskController@update");

    Route::put("/time/{id}", "App\Project\Http\Controllers\TimeController@store");

    Route::delete("/tasks/{id}", "App\Project\Http\Controllers\TaskController@destory");

    Route::delete("/projects/{id}", "App\Project\Http\Controllers\ProjectController@destroy");

});
