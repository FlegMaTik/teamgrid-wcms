<?php namespace App\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTimesTable extends Migration
{
    public function up()
    {
        Schema::create('app_project_times', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('task_id')->index();
            $table->integer('user_id')->index();
            $table->integer('created_by_id')->index();
            $table->dateTime('started_at');
            $table->dateTime('stopped_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_project_times');
    }
}
