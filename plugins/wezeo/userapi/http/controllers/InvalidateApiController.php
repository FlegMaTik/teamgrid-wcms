<?php namespace Wezeo\UserApi\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Wezeo\UserApi\Facades\JWTAuth;
use Wezeo\UserApi\Classes\UserApiHook;

class InvalidateApiController extends UserApiController
{
    public function handle()
    {
        $response = [];

        $user = JWTAuth::getUser();

        $token = JWTAuth::parseToken()->getToken();
        JWTAuth::invalidate($token);

        Event::fire('wezeo.userapi.afterInvalidate', [$user]);

        $response = [
            'success' => true
        ];

        return $afterProcess = UserApiHook::hook('afterProcess', [$this, $response], function () use ($response) {
            return response()->make([
                'response' => $response,
                'status' => 200
            ], 200);
        });
    }
}
