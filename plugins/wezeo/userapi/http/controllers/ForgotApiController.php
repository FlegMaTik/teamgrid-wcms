<?php namespace Wezeo\UserApi\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Models\User;
use Wezeo\UserApi\Classes\UserApiHook;

class ForgotApiController extends UserApiController
{
    public function handle()
    {
        $response = [];

        $params = [
            'email' => input('email'),
        ];

        $user = User::where('email', $params['email'])->firstOrFail();

        if (!$user->is_activated) {
            throw new ApplicationException('User not activated');
        }

        $this->sendResetPasswordCode($user);

        $response = [
            "success" => true
        ];

        return $afterProcess = UserApiHook::hook('afterProcess', [$this, $response], function () use ($response) {
            return response()->make([
                'response' => $response,
                'status' => 200
            ], 200);
        });
    }

    protected function sendResetPasswordCode($user)
    {
        $resetPasswordCode = $user->getResetPasswordCode();
        return Event::fire('wezeo.userapi.sendResetPasswordCode', [$user, $resetPasswordCode], true);
    }
}
