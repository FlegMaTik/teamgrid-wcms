<?php namespace Wezeo\UserApi\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Wezeo\UserApi\Classes\UserApiHook;
use Wezeo\UserApi\Facades\JWTAuth;

class InfoApiController extends UserApiController
{
    public function handle()
    {
        $response = [];

        $user = JWTAuth::getUser();

        Event::fire('wezeo.userapi.beforeReturnUser', [$user]);

        $userResourceClass = config('wezeo.userapi::resources.user');
        $response = [
            'user' => new $userResourceClass($user),
        ];

        return $afterProcess = UserApiHook::hook('afterProcess', [$this, $response], function () use ($response) {
            return response()->make([
                'response' => $response,
                'status' => 200
            ], 200);
        });
    }
}
