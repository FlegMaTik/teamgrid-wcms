<?php

Route::group([
    'prefix' => config('wezeo.userapi::routes.prefix'),
    'middleware' => config('wezeo.userapi::routes.middlewares', [])
], function () {
    $actions = config('wezeo.userapi::routes.actions', []);
    foreach ($actions as $action) {
        $methods = $action['method'];
        if (!is_array($methods)) {
            $methods = [$methods];
        }

        foreach ($methods as $method) {
            Route::{$method}($action['route'], $action['controller'])->middleware($action['middlewares']);
        }
    }
});
